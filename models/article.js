const mongoose = require('mongoose')

const articleSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },

    createdOn: {
        type: Date,
        default: new Date()
    },

})

//model

module.exports = mongoose.model('Article', articleSchema);