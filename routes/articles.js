const express = require('express');
const route = express.Router();
const ArticleController = require('../controller/articles');

route.post('/create', (req, res) => {

    ArticleController.addArticle(req.body).then(result => res.send(result))
})


module.exports = route;